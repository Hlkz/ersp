const express = require('express')
const cors = require('cors')
const playersRouter = require('./routes/players')
const requestData = require('./util/request-data')

const app = express()
app.use(cors({ origin: '*' }))
app.use(express.json())
app.use(
  express.urlencoded({
    extended: true,
  })
)

app.get('/', (req, res) => {
  res.json({ message: 'ok' })
})

app.use('/players', playersRouter)

/* Error handler middleware */
app.use((err, req, res, next) => {
  const statusCode = err.statusCode || 500
  console.error(err.message, err.stack)
  res.status(statusCode).json({ message: err.message })
  return
})

app.requestData = requestData

module.exports = app
