const express = require('express')
const router = express.Router()
const Players = require('../services/players')

router.get('/', async function (req, res, next) {
  try {
    res.status(200).json(await Players.getMultiple(req.query.page))
  } catch (err) {
    console.error(`Error while getting players `, err.message)
    next(err)
  }
})

router.get('/:id', async function (req, res, next) {
  try {
    const body = await Players.getSingle(req.params.id)
    const code = body?.data ? 200 : 404
    res.status(code).json(body)
  } catch (err) {
    console.error(`Error while getting player by id `, err.message)
    next(err)
  }
})

module.exports = router
