## Ersp endpoint

### Demo

See in action : [/players](https://f981u5v7oe.execute-api.eu-west-3.amazonaws.com/latest/players)

### Quick Start

You need to install Node.js first.

Install dependencies:
```bash
$ npm install
```

Start the server for development:
```bash
$ npm run dev
```

Run the tests:
```bash
$ npm run test
```

Deploy to AWS Lambda:
```bash
npx claudia update
```
