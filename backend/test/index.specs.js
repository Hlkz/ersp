const supertest = require('supertest')
const assert = require('assert')
const is = require('../util/is')
const fakePlayers = require('./fake-players')

const app = require('../app')
app.requestData = async () => fakePlayers

describe("GET /players/", async () => {

  const api = supertest(app)
  let err, res
  before(function (done) {
    api.get('/players/').end((_err, _res) => {
      err = _err
      res = _res
      done()
    })
  })

  it("should have status code 200", done => {
    assert(res.statusCode === 200)
    done()
  })
  it("should return a sorted players array", done => {
    assert(is.obj(res.body))
    assert(is.arr(res.body.data))
    assert(res.body.data.length == 2)
    assert(res?.body?.data?.[0]?.id == 17)
    assert(res?.body?.data?.[1]?.id == 102)
    done()
  })
})

describe("GET /player/:id", async () => {
  const testValidId = 17
  const testUnexistantId = 18

  const api = supertest(app)
  let err, res
  before(function (done) {
    api.get('/players/'+testValidId).end((_err, _res) => {
      err = _err
      res = _res
      done()
    })
  })
  it("should have status code 200", done => {
    assert(res.statusCode === 200)
    done()
  })
  it("should return the correct player object", done => {
    assert(is.obj(res.body))
    assert(is.obj(res.body.data))
    assert(res.body.data.id == testValidId)
    done()
  })

  it("should have status code 404 when no match", done => {
    api.get('/players/'+testUnexistantId).end((err, res) => {
      assert(res.statusCode === 404)
      done()
    })
  })
})
