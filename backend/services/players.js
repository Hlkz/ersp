
async function getMultiple() {
  const app = require('../app')
  const data = await app.requestData()
  const players = data.players
  players.sort((a, b) => a.id - b.id)
  return { data: players }
}

async function getSingle(id) {
  const app = require('../app')
  const data = await app.requestData()
  const players = data.players
  const player = players.find(e => e.id == id)
  return { data: player }
}

module.exports = {
  getMultiple,
  getSingle,
}
