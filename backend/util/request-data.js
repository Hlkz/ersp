
const dataEndPoint = 'https://eurosportdigital.github.io/eurosport-node-developer-recruitment/headtohead.json'

async function requestData() {
  const fetch = await import('node-fetch').then(e => e.default)
  const settings = { method: 'Get' }
  const res = await fetch(dataEndPoint, settings)
  const json = await res.json()
  return json
}

module.exports = requestData
