
//
// Types
const isStr = str => typeof str === 'string'
const isNum = num => typeof num === 'number' && !isNaN(num)
const isFn = fn => typeof fn === 'function'
const isObj = obj => obj && (typeof obj === 'object')
const isArr = Array.isArray || (arr => arr && arr.constructor === Array)
const isBool = b => b === true || b === false
const isDef = val => val !== undefined
const isUndef = val => val === undefined

const isBuffer = e => e && (typeof Buffer !== undefined) && (e instanceof Buffer)
const isDate = d => !!d && typeof d.getMonth === 'function'
const isThenable = fn => fn && isFn(fn.then)
const isPromise = fn => isThenable(fn) && isFn(fn.catch)

const isElement = x => x instanceof Element
const isChildren = x => isStr(x) || isArr(x) || isElement(x)
const isObserv = obs => isFn(obs) && isFn(obs.set)
const isEvent = ev => isFn(ev.listen) && isFn(ev.broadcast)
const isPrimitive = prim => {
  switch (typeof prim) {
    case 'string':
    case 'number':
    case 'boolean': return true
    default: return false
  }
}
const isFloat = (float => isNum(float) && Math.floor(float) !== float)
const isInt = Number.isInteger || (int => isNum(int) && Math.floor(int) === int)

function isEmpty(obj) {
  if (obj == null) return true
  if (obj.length > 0) return false
  if (obj.length === 0) return true
  if (typeof obj !== "object") return true
  for (var key in obj) if (Object.prototype.hasOwnProperty.call(obj, key)) return false
  return true
}

//
// React
const isClassComponent = c => !!(isFn(c) && !!c.prototype.isReactComponent)
function isReactChildren(el) {
  if (isArr(el)) return !el.length || isReactChildren(el[0])
  else if (isObj(el)) return isClassComponent(el) || React.isValidElement(el)
  return true
}

//
// Env
const isNode = typeof process !== 'undefined' && process.release && process.release.name === 'node'
const isElectron = !!(typeof window !== 'undefined' && window && window.process && window.process.type)
const isReactNative = (typeof navigator != 'undefined' && navigator.product == 'ReactNative')
const isRendererProcess = isElectron && ((typeof process === 'undefined') || !process || process.type === 'renderer')
const isMainProcess = isElectron && !isRendererProcess

// Platforms
//   aix darwin freebsd linux openbsd sunos win32
const isWin = typeof process !== 'undefined' && process.platform === 'win32'
const isMac = typeof process !== 'undefined' && process.platform === 'darwin'
const isLinux = typeof process !== 'undefined' && process.platform === 'linux'


const is = {
  // types
  isStr,
  isNum,
  isFn,
  isObj,
  isArr,
  isBool,
  isDef,
  isUndef,
  //
  isBuffer,
  isDate,
  isThenable,
  isPromise,
  //
  isElement,
  isChildren,
  isObserv,
  isEvent,
  isPrimitive,
  isInt,
  isFloat,
  isEmpty,
  // react
  isClassComponent,
  isReactChildren,
  // env
  isNode,
  isElectron,
  isRendererProcess,
  isMainProcess,
  isReactNative,
  isWin,
  isMac,
  isLinux,
}

Object.keys(is).forEach(key => is[key.slice(2).toLowerCase()] = is[key])

module.exports = is
