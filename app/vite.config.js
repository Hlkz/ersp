import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    outDir: './build'
  },
  // resolve: {
  //   alias: [
  //     { find: './runtimeConfig', replacement: './runtimeConfig.browser' },
  //     { find: '@', replacement: '/src' },
  //   ],
  // },
  server: {
    proxy: {
      '/api': {
        target: 'http://localhost:3000',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ''),
      },
    }
  },
  plugins: [
    react(),
  ],
  optimizeDeps: {
    exclude: [
    ]
  }
})
