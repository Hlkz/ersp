import { server } from './server'
import { setupStore } from '../store'
import apiSlice from '../features/api/apiSlice'

const store = setupStore({})

beforeAll(() => { server.listen() })
afterEach(() => {
  server.resetHandlers()
})
afterAll(() => server.close())
