import { rest } from 'msw'
import fakePlayers from './fake-players.json'
import config from '../../../config'
const { dataEndPoint } = config

const handlers = [
  rest.get(dataEndPoint + '/players', (req, res, ctx) => {
    const mockApiResponse = {
      data: fakePlayers
    }
    return res(ctx.json(mockApiResponse))
  }),
  rest.get(dataEndPoint + '/players/17', (req, res, ctx) => {
    const mockApiResponse = {
      data: fakePlayers[0]
    }
    return res(ctx.json(mockApiResponse))
  }),
  rest.get(dataEndPoint + '/players/18', (req, res, ctx) => {
    const mockApiResponse = {}
    return res(ctx.status(404), ctx.json(mockApiResponse))
  }),
]

export { handlers }