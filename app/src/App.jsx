import React from 'react'
import Versus from './features/players/Versus'
import { useGetPlayersQuery } from './features/api/apiSlice'
import './App.css'

function App() {
  const { data: players, isLoading } = useGetPlayersQuery()
  const playerIds = React.useMemo(() => players?.map(e => e.id), [players])

  return (
    <div className="App">
      <div className="header">EuroStat</div>
      {!!playerIds && <Versus playerIds={playerIds} />}
      {!playerIds && !isLoading &&
        <div className="errorMessage">
          Une erreur est survenue lors du chargement de la page. Veuillez-réessayer plus tard.
        </div>
      }
    </div>
  )
}

export default App