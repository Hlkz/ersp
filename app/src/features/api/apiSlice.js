import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import config from '../../../config'

export const apiSlice = createApi({
  reducerPath: 'api',
  baseQuery: fetchBaseQuery({
    baseUrl: config.dataEndPoint,
  }),
  endpoints: builder => ({
    getPlayers: builder.query({
      query: () => '/players',
      transformResponse: (res, meta, arg) => res.data,
    }),
    getPlayer: builder.query({
      query: postId => `/players/${postId}`,
      transformResponse: (res, meta, arg) => res.data,
    })
  })
})

export const { useGetPlayersQuery, useGetPlayerQuery } = apiSlice
