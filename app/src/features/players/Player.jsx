import React from 'react'
import { useGetPlayerQuery } from '../api/apiSlice'
import './Player.css'

function Player(props) {
  const { playerId, changePlayer } = props
  const { data: player, isFetching } = useGetPlayerQuery(playerId)
  const playedTime = React.useMemo(() => randomIntBetween(80,200), [playerId])

  if (!player && isFetching) // Loader
    return <div className="Player"></div>

  if (!player && !isFetching) // Error
    return <div className="Player">
      <span className="errorMessage">
        Une erreur est survenue lors de la récupération des données du joueur. Veuillez réessayer plus tard.
      </span>
    </div>

  const { firstname, lastname, picture } = player
  const fullname = firstname + ' ' + lastname
  const { rank, points, height, weight, age } = player.data

  return (
    <div className="Player">
      <div className="picture" style={{ backgroundImage: `url(${picture})` }} />
      <div className="fullname">{fullname}</div>
      <div className="rank">Rank: {rank}</div>
      <h2>Stats</h2>
      <dl>
        <div>
          <dt>Points:</dt>
          <dd>{points}</dd>
        </div>
        <div>
          <dt>Height:</dt>
          <dd>{formatHeight(height)}</dd>
        </div>
        <div>
          <dt>Weight:</dt>
          <dd>{formatWeight(weight)}</dd>
        </div>
        <div>
          <dt>Age:</dt>
          <dd>{formatAge(age)}</dd>
        </div>
      </dl>
      <div className="totalPlayedTime">
        Total played time:
        <span>{playedTime} hours</span>
      </div>
      <div className="buttons">
        <button onClick={changePlayer}>Change</button>
        <button className="arrow">See details</button>
      </div>
    </div>
  )
}

function formatHeight(cm) {
  let m = Math.floor(cm/100)
  let rest = cm - 100*m
  rest = !rest ? '' : ''+rest
  if (rest && rest.length == 1) rest = '0'+rest
  return m + 'm' + rest
}

function formatWeight(grams) {
  return Math.round(grams/1000) + 'kg'
}

function formatAge(age) {
  return age + ' ans'
}

function randomIntBetween(min, max) {
  return min + Math.round(Math.random() * (max - min))
}

export default Player
