import React from 'react'
import Player from './Player'

function Versus(props) {
  const { playerIds } = props // playerIds should never change

  //
  // Arbitrary id logic
  //

  const getRandomId = excludeIds => {
    excludeIds = excludeIds || []
    const ids = playerIds.filter(e => !excludeIds.includes(e))
    return ids[Math.floor(Math.random()*ids.length)]
  }
  const [playerId1, setPlayerId1] = React.useState(getRandomId())
  const playerId1Ref = React.useRef()
  playerId1Ref.current = playerId1

  const [playerId2, setPlayerId2] = React.useState(getRandomId([playerId1]))
  const playerId2Ref = React.useRef()
  playerId2Ref.current = playerId2

  const getNewRandomId = () => {
    const playerId1 = playerId1Ref.current
    const playerId2 = playerId2Ref.current
    return getRandomId([playerId1, playerId2])
  }
  const changePlayer1 = React.useCallback(() => setPlayerId1(getNewRandomId()), [])
  const changePlayer2 = React.useCallback(() => setPlayerId2(getNewRandomId()), [])

  //

  return <div className="Versus">
    <span>vs</span>
    <div className="players">
      <Player {...{ key: 1, playerId: playerId1, changePlayer: changePlayer1 }} />
      <Player {...{ key: 2, playerId: playerId2, changePlayer: changePlayer2 }} />
    </div>
  </div>
}

export default Versus
