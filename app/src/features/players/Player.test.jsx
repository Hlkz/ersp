import React from 'react'
import '../../test/setup'
import { server } from '../../test/server'
import { renderWithProviders } from '../../test/test-utils'
import { screen, waitFor } from '@testing-library/react'
import Player from './Player'
import assert from 'assert'

describe('Player', () => {

  it('renders while loading', async () => {
    const { container } = renderWithProviders(<Player playerId={17} />)
    assert(container.firstChild.className == 'Player')
  })

  it('handles good response', async () => {
    const { container } = renderWithProviders(<Player playerId={17} />)
    await waitFor(() => screen.getByText('Rafael Nadal'))
    const img = container.getElementsByClassName('picture')[0]
    assert(img)
    let imgStyle = window.getComputedStyle(img)
    expect(imgStyle.backgroundImage).toBe('url(https://i.eurosport.com/_iss_/person/pp_clubteam/large/435121.jpg)')
  })

  it('handles error response', async () => {
    const { container } = renderWithProviders(<Player playerId={18} />)
    await waitFor(() => screen.findByText(/Une erreur est survenue/))
  })
})
