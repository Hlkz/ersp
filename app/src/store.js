import { configureStore } from '@reduxjs/toolkit'
// import playersReducer from './features/players/playersSlice'
import { apiSlice } from './features/api/apiSlice'

function setupStore(preloadedState) {
  return configureStore({
    reducer: {
      // players: playersReducer,
      [apiSlice.reducerPath]: apiSlice.reducer
    },
    middleware: getDefaultMiddleware =>
      getDefaultMiddleware().concat(apiSlice.middleware),
    preloadedState,
  })
}

export {
  setupStore,
}