## Ersp react app

### Demo

[Deployed app](https://master.d2i3ozum4um49l.amplifyapp.com/)

### Quick Start

You need to install Node.js first.

Install dependencies:
```bash
$ npm install
```

Start the vite app in dev mode:
```bash
$ npm run dev
```

Start the app locally in production mode:
```bash
$ npm run preview
```

Build a production version:
```bash
$ npm run build
```

Run the tests:
```bash
$ npm run test
```
